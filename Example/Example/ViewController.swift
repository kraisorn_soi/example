//
//  ViewController.swift
//  Example
//
//  Created by Kraisorn Soisaku on 24/5/2564 BE.
//

import UIKit
import UIConsent

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func doPresentConsent(_ sender: UIButton) {
        UIConsent.shared.presentConsent(in: self, animated: true, completion: nil)
    }

}

