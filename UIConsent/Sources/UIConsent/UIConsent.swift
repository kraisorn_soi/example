#if canImport(UIKit)

import UIKit

public class UIConsent {
    public static let shared = UIConsent()
    
    func getView(with identifier: String) -> UIViewController {
        return UIStoryboard(name: "Consent", bundle: Bundle.module).instantiateViewController(withIdentifier: identifier)
    }
}

public extension UIConsent {
    func presentConsent<VC: UIViewController>(in view: VC, animated: Bool, completion: (() -> Void)?) {
        guard let vc = self.getView(with: "ConsentViewController") as? ConsentViewController else {
            print("----Can't instaintate view---")
            return
        }
        view.present(vc, animated: animated, completion: completion)
    }
}

#endif
