//
//  File.swift
//  
//
//  Created by Kraisorn Soisaku on 24/5/2564 BE.
//

#if canImport(UIKit)

import UIKit
import XService
import User

class ConsentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("--uid--", User.shared.id ?? "")
        print("--uname--", User.shared.name ?? "")
    }
    
    @IBAction func doCallService(_ sender: UIButton) {
        XService.shared.request {
            print("---- user do request ---")
            print("--uid--", User.shared.id ?? "")
            print("--uname--", User.shared.name ?? "")
        }
    }
}

#endif
