import Foundation

public class User: NSObject {
    public static let shared = User()
    
    public private(set) var id: String?
    public private(set) var name: String?
    
    public override init() {
        super.init()
        
        self.id = "xid"
        self.name = "xname"
    }
}

public extension User {
    func updateData(id: String?, name: String?) {
        self.id = id
        self.name = name
    }
}
