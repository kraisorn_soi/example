import Foundation
import User

public class XService {
    public static let shared = XService()
}

public extension XService {
    func request(completion: @escaping () -> Void) {
        User.shared.updateData(id: "nId", name: "nName")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            completion()
        }
    }
}
